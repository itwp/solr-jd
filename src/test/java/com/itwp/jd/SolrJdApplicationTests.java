package com.itwp.jd;

import com.itwp.jd.pojo.Content;

import org.apache.http.impl.bootstrap.HttpServer;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@SpringBootTest
class SolrJdApplicationTests {

    @Autowired
    private SolrClient solrClient;

    /**
     * 添加 对Solr索引库进行添加功能
     */
    @Test
    void insert() throws IOException, SolrServerException {


        Content content = new Content();
        content.setId(UUID.randomUUID().toString());
        content.setTitle("Vue.js，我是中国人，我爱中国！");
        content.setImg("测试.jpg");
        content.setPrice("99.99");
        content.setShop("asjdkasjdkasjdka");
        UpdateResponse response = solrClient.addBean(content);
        //UpdateResponse response = solrClient.addBean(content);
        System.out.println("索引添加成功！");
        solrClient.commit();
    }



    /**
     * 删除索引（根据条件来删除）
     */
    @Test
    public void delIndexByQuery() throws Exception {

        //client.deleteById("30001");
        //根据条件删除
        solrClient.deleteByQuery("*:*");
        //全部删除
        //client.deleteByQuery("*:*");
        System.out.println("删除成功！");
        solrClient.commit();
    }

}
