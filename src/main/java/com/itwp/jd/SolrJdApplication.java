package com.itwp.jd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolrJdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SolrJdApplication.class, args);
    }

}
