package com.itwp.jd.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.beans.Field;

/**
 * Created by Carl Wu on 2020/4/28 2:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Content {
    @Field("id")
    private String  id;
    @Field("c_title")
    private String title;
    @Field("c_img")
    private String img;
    @Field("c_price")
    private String price;
    @Field("c_shop")
    private String shop;

}