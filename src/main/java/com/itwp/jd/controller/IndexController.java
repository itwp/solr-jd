package com.itwp.jd.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Carl Wu on 2020/4/28 2:04
 */
@Controller
public class IndexController {


    @GetMapping({"/","/index"})
    public String index(){
        return "index";
    }
}